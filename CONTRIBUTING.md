## Working with the Linter

To ensure code quality and adherence to coding standards, we use `ruff` as our primary tool for linting. Before committing your changes, please make sure your code passes all lint checks without errors.

You can run the linter locally using the following command:

```bash
ruff .
```

To automatically fix some of the errors, you can use the `--fix` option:

```bash
ruff . --fix
```
