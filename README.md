## Repository Management Methodology

In this project, we adhere to the following practices:

- All changes are made through Merge Requests.
- Each task is performed in a separate branch.
- Before creating a Merge Request, make sure your code has passed all linter checks.
- We value clear, readable code that adheres to community standards and guidelines.

### Code Quality

To maintain high code quality, we use `ruff` for linting. It helps us identify and fix issues related to code formatting, unused code, potential errors, and style inconsistencies.

### Pre-commit Hooks

We use pre-commit hooks to automatically check and fix code before it is committed. This ensures that all code complies with our coding standards. To set up pre-commit hooks, run the following command after cloning the repository:

```bash
pre-commit install
```

This command will install all necessary hooks as defined in `.pre-commit-config.yaml`.


## Docker Configuration

### Building the Docker Image
To build the Docker image for this project, run the following command:
```bash
docker build -t ml_project_image .
```

### Running the Docker Container
To run the Docker container from the image, use:
```bash
docker run -p 4000:80 ml_project_image
```
